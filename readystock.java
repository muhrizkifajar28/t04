class readystock extends kue{

    private double jumlah;
    private double berat;

    public readystock(String name, double price, double jumlah) {
        super(name, price);
        this.jumlah = jumlah;
    }

    // implementasi method abstract pada kelas induk (kue)
    
    @Override
    public double Berat() {
        return berat;
    }
    
    @Override
    public double Jumlah() {
        return jumlah;
    }
    
    @Override
    public double hitungHarga() {
        return getPrice() * Jumlah() * 2;
    }
}