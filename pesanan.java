class pesanan extends kue{

    private double berat;
    private double jumlah;

    public pesanan(String name, double price, double berat) {
        super(name, price);
        this.berat = berat;
    }

    // implementasi method abstract pada kelas induk (kue)

    @Override
    public double Berat() {
        return berat;
    }
    
    @Override
    public double Jumlah() {
        return jumlah;
    }
    
    @Override
    public double hitungHarga() {
        return getPrice() * Berat();
    }
}